# taxonomy-service

Microservizio dedicato alla gestione delle tassonomie previste per varie entità della piattaforma.

Il servizio è multi-tenant perché la maggioranza delle istanze degli enti
condividono le tassonomia dei comuni, in alcuni casi potrebbe essere ammissibile una modifica di una parte della tassonomia, ma sono comunque modifiche a un fetta molto piccola. In quel caso l'ente vedrà la tassonomia generale e in piu' la sua modifica locale.

Esempio:

 - tassonomia degli uffici: è la stessa per tutti e serve a catalogare i gruppi di utenti
 - argomenti del sito comunale: è la stessa x tutti gli enti di tipo comune ma potrebbe essere modificata in una percentuale controllata

## Tassonomie da supportare

A livello di [architettura del sito](https://docs.google.com/spreadsheets/d/1D4KbaA__xO9x_iBm08KvZASjrrFLYLKX/edit#gid=335720294) comunale queste sono le tassonomie da supportare:

- argomenti del sito comunale
- categorie dei servizi
- destinatari dei servizi
- tassonomia dei luoghi
- tassonomia delle unità organizzative
- eventi della vita delle persone
- eventi della vita delle imprese

## Funzionalità

- ogni tassonomia e ogni termine hanno un owner, che detiene il diritto di modificare il contenuto dell'oggetto; tutte le tassonomie saranno invece libere in lettura;
- ogni termine è traducibile in più lingue;
- in ogni tassonomia l'owner può decidere se renderla estendibile da altri o meno e se si in che misura (soglia di % di termini originali);
- in ogni tassonomia, se l'admin lo rende possibile, ogni altro utilizzatore può aggiungere propri termini, che saranno visibili solo a quell'utilizzatore;
- Ogni termine può essere valido, deprecato o archiviato (da specificare eventuali date necessarie);
  - se _deprecato_ non sarà più utilizzabile per aggiungerlo a un oggetto ma ancora resta visibile per gli oggetti che lo stanno usando; 
  - se _deprecato_ potrebbe esserci una data oltre la quale il termine non sarà più disponibile;
  - se _archiviato_ non sarà più utilizzabile, chi lo usa dovrà rimuovere quel riferimento alla prima occasione utile; da valutare se restituire una stringa di errore o vuota invece del termine archiviato.

Alla configurazione del tenant vengono configurate le tassonomie necessarie alla piattaforma: un comune avrà tutte le tassonomie del modello comunale. Altri tipi di tenant avranno tassonomie custom vuote oppure una tassonomia di default diversa.

## API

### Lettura singola tassonomia

`/taxonomy/${id}`


### lettura di un singolo termine

GET `/taxonomy/${taxonomy_id}/terms/${id}`

```json
{
  "id": "123456-1234-1234-...",
  "owner": {
    "id": "123456-1234-1234-...",
    "name": "",
    "url": ""
  },
  "taxonomy": {
    "id": "",
    "name": "",
    "url": 
  },
  "label": "",
  "alternative_labels": "",
  "description": "",
  "level": 1,
  "parent_id": "",
  "semantic_links":
    {
        "ontopia": "https://w3id.org/italia/controlled-vocabulary/classifications-for-public-services/public-services-subject-matters/13",
        "eurovoc": ""
    },
}
```

### Lista dei termini

GET `/taxonomy/${taxonomy_id}/terms?parent_id=${parent_id}`

per la generazione di select:

GET `/taxonomy/${taxonomy_id}/terms?format=flattened`

```json
{
  "metadata": {},
  "data": [
    {
      "id": "123456-1234-1234-...",
      "owner_id": "123456-1234-1234-...",
      "label": "",
      "alternative_labels": "",
      "level": 1,
      "parent_id": "",
    },
    {
      ...  
    }
  ]
}
```
